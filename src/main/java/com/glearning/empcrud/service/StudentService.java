package com.glearning.empcrud.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.glearning.empcrud.model.Student;

@Service
public class StudentService {
	
	private List<Student> students = new ArrayList<>();
	
	public List<Student> fetchStudents(){
		return this.students;
	}
	
	public Student saveStudent(Student student) {
		this.students.add(student);
		return student;
	}
	
	public void deleteStudentById(int id) {
		for(int i = 0; i < students.size(); i++) {
			if(students.get(i).getGrade() == id) {
				students.remove(i);
			}
		}
	}

}
