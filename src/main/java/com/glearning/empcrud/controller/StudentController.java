package com.glearning.empcrud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.glearning.empcrud.model.Student;
import com.glearning.empcrud.service.StudentService;

//we will be building a crud application
 /*
  * crud - stands for Create -> Read -> Update -> Delete
  */

@Controller
@RequestMapping("/student")
public class StudentController {
	
	@Autowired
	private StudentService studentService;
	
	@RequestMapping("/")
	public String welcome() {
		return "home";
	}
	
	@RequestMapping(method = RequestMethod.GET, value="/register")
	public String registerUser(Model model) {
		model.addAttribute("student", new Student());
		return "register";
	}
	
	@RequestMapping(method = RequestMethod.POST, value="/submit")
	public  String registerStudent(@ModelAttribute("student")Student student) {
		this.studentService.saveStudent(student);
		return "redirect:/student/list";
	}

	@GetMapping("/list")
	public String listStudents(Model model) {
		List<Student> students = this.studentService.fetchStudents();
		model.addAttribute("students", students);
		return "home";
	}
	
	@GetMapping("/{id}")
	public String deleteStudent(@PathVariable("id") int id) {
		this.studentService.deleteStudentById(id);
		return "redirect:/student/list";
	}


}
