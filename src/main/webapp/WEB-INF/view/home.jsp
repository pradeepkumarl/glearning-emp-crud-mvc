<%@ page contentType="text/html;charset=UTF-8" language="java" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
  <title>List of all Students</title>
</head>
<body>

<h1>List of all Studnets</h1>

<h2>Register new Student</h2>
<a href="register">Register New Student!!</a>

<h2>List all the Students</h2>

  <ul>
      <c:forEach var="student" items="${students}">
       <li>First Name: ${student.firstName}</li>
       <li>Last Name: ${student.lastName}</li>
       <li>Grade: ${student.grade}</li>
      </c:forEach>
  </ul>
    <c:if test="${students.size() > 2}">
    <h2>Awesome !!. We have many students ....</h2>
    </c:if>
</body>
</html>