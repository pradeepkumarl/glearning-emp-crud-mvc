<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Success</title>
    <style type="text/css">
        .error {
            color: red;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>

<div class="container">
  <form:form action="submit" method="post" modelAttribute="student">
        <div class="mb-3">
            <label class="form-label">First Name</label>
            <form:input path="firstName" placeholder="Enter your name" class="form-control"/>
        </div>
        <div class="mb-3">
            <label class="form-label">Last Name</label>
            <form:input path="lastName" placeholder="Enter your name" class="form-control"/>
        </div>
        <div class="mb-3">
            <label class="form-label">Grade</label>
            <form:input path="grade" placeholder = "Enter your grade" class="form-control"/>
        </div>
        <button type="submit" class="bt-primary">Submit</button>
    </form:form>
</div>
</body>
</html>